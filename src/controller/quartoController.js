module.exports = class QuartoController {
    static quartos = []; // Armazenamento temporário dos quartos cadastrados

    static async postQuarto(req, res) {
        try {
            const { numero, andar, tipoQuarto, valorDiaria } = req.body;

            // verifica se todos os campos estão preenchidos
            if (!numero || !andar || !tipoQuarto || !valorDiaria) {
                res.status(400).json({ message: 'Campos obrigatórios não preenchidos.' });
                return;
            }

            // valida o numero do quarto
            if (numero < 1 || numero > 180) {
                res.status(400).json({ message: 'Número do quarto inválido. Deve ser entre 1 e 180.' });
                return;
            }

            // valida o andar
            if (andar < 1 || andar > 11) {
                res.status(400).json({ message: 'O andar deve estar entre 1 e 11.' });
                return;
            }

            // valida o valor do quarto
            if (valorDiaria < 50) {
                res.status(400).json({ message: 'Valor do quarto inválido.' });
                return;
            }

            const id = QuartoController.quartos.length + 1; // Gera um ID único para o quarto
            const quarto = { id, numero, andar, tipoQuarto, valorDiaria };
            QuartoController.quartos.push(quarto); // Adiciona o quarto ao armazenamento

            res.status(201).json({ message: 'Quarto cadastrado com sucesso.', id });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Erro interno do servidor.' });
        }
    }

   static async getQuartoID(req, res) {
        try {
            const { id } = req.params;

            if (id) {
                const quarto = QuartoController.quartos.find(quarto => quarto.id === parseInt(id));
                if (!quarto) {
                    res.status(404).json({ message: 'Quarto não encontrado.' });
                    return;
                }
                res.status(200).json(quarto);
            } else {
                res.status(200).json(QuartoController.quartos);
            }
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Erro interno do servidor.' });
        }
    }
    

    static async getQuarto(req, res) {
        try {
            res.status(200).json(QuartoController.quartos); // Retorna todos os quartos cadastrados
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Erro interno do servidor.' });
        }
    }


    static async putQuarto(req, res) {
        try {
            const { id } = req.params;
            const { numero, andar, tipoQuarto, valorDiaria } = req.body;

            // Encontra o quarto pelo ID
            const quarto = QuartoController.quartos.find(quarto => quarto.id === parseInt(id));

            if (!quarto) {
                res.status(404).json({ message: 'Quarto não encontrado.' });
                return;
            }

            // Atualiza os dados do quarto
            Object.assign(quarto, { numero, andar, tipoQuarto, valorDiaria });

            res.status(200).json({ message: 'Quarto atualizado com sucesso.', id });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Erro interno do servidor.' });
        }
    }

    static async deleteQuartoId(req, res) {
        const { id } = req.params;

        try {
            const index = QuartoController.quartos.findIndex(quarto => quarto.id === parseInt(id));

            if (index === -1) {
                res.status(404).json({ message: 'Quarto não encontrado.' });
                return;
            }

            // Remove o quarto do armazenamento
            QuartoController.quartos.splice(index, 1);

            res.status(200).json({ message: 'Quarto removido com sucesso.', id });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Erro interno do servidor.' });
        }
    }
};
