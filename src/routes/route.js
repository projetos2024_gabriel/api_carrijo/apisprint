const router = require('express').Router()
const usuarioController = require('../controller/usuarioController')
const quartoController = require('../controller/quartoController')
const dbController = require('../controller/dbController')

router.post('/cadastroUser/', usuarioController.postUser)
router.put('/atualizarUser/', usuarioController.putUser)
router.delete('/deleteUser/:id', usuarioController.deleteUser)
router.post('/login/', usuarioController.logUser)
router.post('/signin', usuarioController.signInUser)

//rotas criadas para post, put, e delete
router.post('/postQuarto/', quartoController.postQuarto)
router.put('/putQuarto/:id', quartoController.putQuarto)
router.delete('/deleteQuarto/:id', quartoController.deleteQuartoId)
router.get('/listarQuartoID/:id', quartoController.getQuartoID)
router.get('/listarQuarto/', quartoController.getQuarto)





router.get('/tablesNames/', dbController.getTablesNames)
router.get('/tablesDescriptions/', dbController.getTablesDescriptions)

module.exports = router;