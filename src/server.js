const app = require('./index')

app.listen(5000, () => {
    console.log('Servidor roteando na porta 5000.');
    console.log('Acesse nosso site pelo http://localhost:5000/teste/');
})